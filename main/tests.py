from django.test import TestCase, Client
from django.urls import resolve
from .views import home, tambah_kegiatan, tambah_orang
from .models import Person, Kegiatan

# Create your tests here.
class TestIndex(TestCase):
	def test_index_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_index_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, home)

	def test_index_using_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'main/home.html')

class TestAddEvent(TestCase):
	def test_add_Event_url_is_exist(self):
		response = Client().get('/tambah_kegiatan/')
		self.assertEqual(response.status_code, 200)

	def test_add_Event_index_func(self):
		found = resolve('/tambah_kegiatan/')
		self.assertEqual(found.func, tambah_kegiatan)

	def test_add_Event_using_template(self):
		response = Client().get('/tambah_kegiatan/')
		self.assertTemplateUsed(response, 'main/tambah_kegiatan.html')

	def test_Event_model_create_new_object(self):
		acara = Kegiatan(nama_kegiatan ="abc")
		acara.save()
		self.assertEqual(Kegiatan.objects.all().count(), 1)

	def test_add_KegiatanPost(self):
		response = Client().post('/tambah_kegiatan/', {"nama_kegiatan" : "abcd", "deskripsi" : "yeyeyeye"})
		self.assertEqual(response.status_code, 302)

class TestAddMember(TestCase):
	def setUp(self):
		acara = Kegiatan(nama_kegiatan ="abc", deskripsi='yeeyeye')
		acara.save()

	def test_add_Member_url_is_exist(self):
		response = Client().post('/tambah_orang/1/', data={'nama_orang':'ghifari'})
		self.assertEqual(response.status_code, 302)
	
	def test_add_Person_Post(self):
		response = Client().post('/tambah_orang/1/', {"nama_orang" : "yeyeye"})
		self.assertEqual(response.status_code, 302)
	

class TestModel(TestCase):
	def test_str_model_event(self):
		kegiatan = Kegiatan.objects.create(nama_kegiatan='pepewe', deskripsi='yeyeye')
		self.assertEqual(kegiatan.__str__(), 'pepewe')

	def test_str_model_member(self):
		kegiatan = Kegiatan.objects.create(nama_kegiatan='pepewe')
		person = Person.objects.create(nama_orang='memberTest', kegiatan=kegiatan)
		self.assertEqual(person.__str__(), 'memberTest')
