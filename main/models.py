from django.db import models

# Create your models here.

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField("Nama", max_length = 50)
    deskripsi = models.TextField("Deskripsi", max_length = 100)

    def __str__(self):
        return self.nama_kegiatan

class Person(models.Model):
    nama_orang = models.CharField("Nama", max_length = 50)
    kegiatan = models.ForeignKey("Kegiatan", on_delete = models.CASCADE)

    def __str__(self):
        return self.nama_orang