from django.shortcuts import render, redirect
from .models import Kegiatan, Person
from .forms import FormKegiatan, FormPerson

def home(request):

    person = Person.objects.all()
    kegiatan = Kegiatan.objects.all()
    context = {'person' : person, 'kegiatan' : kegiatan}  

    return render(request, 'main/home.html', context)


def tambah_kegiatan(request):

    if request.method == "POST":
        form = FormKegiatan(request.POST)

        if form.is_valid():
            form.save()
            return redirect("main:home")

    else:
        form = FormKegiatan

    context = {'form' : form}            
    return render(request, 'main/tambah_kegiatan.html', context)

def tambah_orang(request, id):

    if request.method == "POST":
        form = FormPerson(request.POST)

        form = Person(kegiatan = Kegiatan.objects.get(id=id), nama_orang = form.data['nama_orang'])
        form.save()
        return redirect('main:home')

    else:
        form = FormPerson
        
    context = {'form' : form}
    return render(request, 'main/tambah_orang.html', context)
